from pymongo import MongoClient
import json

client = MongoClient()
db = client.twitter

result = db.tweets.find(
              {"$and": [{"lang":"en"}, {"retweet_count": {"$gte": 0} },
               {"user.followers_count":{"$gte": 500} }  ]},
              {"text" : 1, "lang" : 1, "retweet_count" : 1, "user.followers_count":1})
for document in  result:
    print(document)


