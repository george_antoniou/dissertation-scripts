#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from cassandra.cluster import Cluster
import csv

cluster = Cluster()
session = cluster.connect('hotel_recommendations')

results = session.execute("""
          CREATE AGGREGATE myAVG(int)
          SFUNC sumState
          STYPE tuple<int,int>
          FINALFUNC avgState
          INITCOND (0,0) 
           """)    
           
for row in results:
    print(row)
    

