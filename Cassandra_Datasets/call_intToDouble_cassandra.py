#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from cassandra.cluster import Cluster
import csv

cluster = Cluster()
session = cluster.connect('hotel_recommendations')

results = session.execute("""
          SELECT intToDouble(children)
          FROM search_data
          WHERE hotel_country = 1 AND user_location_country >= 10
          AND user_location_country <=15 
          LIMIT 100 """)    
           
for row in results:
    print(row)
    

