from cassandra.cluster import Cluster

cluster = Cluster()
session = cluster.connect('hotel_recommendations')

results = session.execute("""
    CREATE TABLE SEARCH_DATA(
       DATE_TIME TEXT,
       SITE_NAME INT,
       POSA_CONTINENT INT,
       USER_LOCATION_COUNTRY INT,
       USER_LOCATION_REGION INT,
       USER_LOCATION_CITY INT,
       ORIG_DEST_DISTANCE DOUBLE,
       USER_ID INT,
       IS_MOBILE INT,
       IS_PACKAGE INT,
       CHANNEL INT,
       CHECKIN_DATE TEXT,
       CHECKOUT_DATE TEXT,
       ADULTS INT,
       CHILDREN INT,
       ROOMS INT,
       DESTINATION_ID INT,
       DESTINATION_TYPE_ID INT,
       HOTEL_CONTINENT INT,
       HOTEL_COUNTRY INT,
       HOTEL_MARKET INT,
       IS_BOOKING INT,
       CNT BIGINT,
       HOTEL_CLUSTER INT,
       PRIMARY KEY (HOTEL_COUNTRY, USER_LOCATION_COUNTRY, USER_ID,DATE_TIME)
       )""")

        
