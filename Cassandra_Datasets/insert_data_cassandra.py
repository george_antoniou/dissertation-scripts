#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from cassandra.cluster import Cluster

cluster = Cluster()
session = cluster.connect('hotel_recommendations')

fp = open('train.csv')
line = fp.readline()
line = fp.readline()

while line:
    data = line.split(',')
    try:
        float(data[6])
    except:
        data[6]=0.0    
    results = session.execute("""
        INSERT INTO data(
        date_time,
        site_name,
        posa_continent, 
        user_location_country,
        user_location_region,
        user_location_city,
        orig_dest_distance,
        user_id,
        is_mobile,
        is_package,
        channel,
        checkin_date,
        checkout_date,
        adults,
        children,
        rooms,
        destination_id,
        destination_type_id,
        hotel_continent,
        hotel_country,
        hotel_market,
        is_booking,
        cnt,
        hotel_cluster)
        VALUES
        (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
         %s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
        """,
        (data[0],int(data[1]),int(data[2]),int(data[3]),int(data[4]),int(data[5]),
         float(data[6]),int(data[7]),int(data[8]),int(data[9]),int(data[10]),            
         data[11],data[12],int(data[13]),int(data[14]),int(data[15]),
         int(data[16]),int(data[17]),int(data[18]),int(data[19]),
         int(data[20]),int(data[21]),int(data[22]),int(data[23])))     
    line = fp.readline()
 
    

