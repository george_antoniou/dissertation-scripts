#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from cassandra.cluster import Cluster
import csv

cluster = Cluster()
session = cluster.connect('hotel_recommendations')

results = session.execute("""
          CREATE FUNCTION intToDouble (input int)
          RETURNS NULL ON NULL INPUT
          RETURNS DOUBLE
          LANGUAGE java
          AS 
          ' return (double)(input);' 
           """)    
           
for row in results:
    print(row)
    

