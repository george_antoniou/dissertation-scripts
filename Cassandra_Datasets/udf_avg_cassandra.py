#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from cassandra.cluster import Cluster
import csv

cluster = Cluster()
session = cluster.connect('hotel_recommendations')

results = session.execute("""
          CREATE OR REPLACE FUNCTION avgState (state tuple<int, int>)
          CALLED ON NULL 
          INPUT RETURNS double
          LANGUAGE java
          AS 
           ' return (double) state.getInt(1)/ (double) state.getInt(0);' 
           """)    
           
for row in results:
    print(row)
    

