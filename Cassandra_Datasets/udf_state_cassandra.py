#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from cassandra.cluster import Cluster
import csv

cluster = Cluster()
session = cluster.connect('hotel_recommendations')

results = session.execute("""
          CREATE OR REPLACE FUNCTION sumState (state tuple<int, int>, value int)
          CALLED ON NULL 
          INPUT RETURNS tuple<int,int>
          LANGUAGE java
          AS 
          ' if (value != null){
                state.setInt(0, state.getInt(0)+ 1);
                state.setInt(1, state.getInt(1) + value.intValue());
            }
            return state;' 
           """)    
           
for row in results:
    print(row)
    

