#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from cassandra.cluster import Cluster
import csv

cluster = Cluster()
session = cluster.connect('hotel_recommendations')

results = session.execute("""
          UPDATE search_data
          SET adults = 2, children = 2
          WHERE hotel_country = 23 AND user_location_country = 1
          AND USER_ID = 213635 AND DATE_TIME = '2014-01-28 22:50:12'""")    
            
for row in results:
    print(row)
    

