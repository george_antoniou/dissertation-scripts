from pymongo import MongoClient
import json

client = MongoClient()
db = client.twitter

result = db.tweets.aggregate(
                [
                 {"$project":{"lang":1}},
                 {"$group":{"_id":"$lang", "total_number":{"$sum":1}}},
                 {"$sort":{"total_number":-1}}, 
                 {"$limit":10}
                ]
               )
for document in  result:
    print(document)


