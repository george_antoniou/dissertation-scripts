#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from cassandra.cluster import Cluster
import csv
import time
import sys
import os.path
import random
import threading
from cassandra.query import BatchStatement

cluster6 = Cluster(['10.0.1.206'])
session6 = cluster6.connect('checkins')
cluster7 = Cluster(['10.0.1.207'])
session7 = cluster7.connect('checkins')
cluster8 = Cluster(['10.0.1.208'])
session8 = cluster8.connect('checkins')

start = time.time()

statement6 = session6.prepare("""
                SELECT *
                FROM data
                WHERE id = ?""")
statement7 = session7.prepare("""
                SELECT *
                FROM data
                WHERE id = ?""")
statement8 = session8.prepare("""
                SELECT *
                FROM data
                WHERE id = ?""")


def read_query(session, statement):
    start = time.time()
    results = []
    
    for i in range(100):
        value = random.randrange(232944168)
        results.append(session.execute(statement, [value]))
                       
    end = time.time()
    print("Read 1K time: " + str(end-start))

t6 = threading.Thread(target=read_query, args=(session6,statement6, ))
t7 = threading.Thread(target=read_query, args=(session7,statement7, ))
t8 = threading.Thread(target=read_query, args=(session8,statement8, ))
t6.start()
t7.start()
t8.start()


