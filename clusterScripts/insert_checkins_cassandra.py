#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from cassandra.cluster import Cluster
import csv
import time
import sys
import os.path

cluster = Cluster(['10.0.1.206'])
session = cluster.connect('checkins')
start = time.time()
row = 0

save_path = '/home/george/scripts/cassandra_times/'

for i in range(3):
    with open ('train.csv', 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        next(reader,None)
        for data in reader:
            results = session.execute("""
                INSERT INTO data(
                ID,
                ROW_ID,
                X, 
                Y,
                TIME,
                ACCURACY,
                PLACE_ID)
                VALUES
                (%s,%s,%s,%s,%s,%s,%s)
                """,
                (row,int(data[0]),float(data[1]),float(data[2]),int(data[3]),
                int(data[4]),int(data[5])))            
                  
            
            row = row + 1
            if (row % 1000000 == 0):
                end = time.time()
                filename = os.path.join(save_path, "row_"+str(row)+".txt")
                output = open (filename, 'w')
                output.write("Row: " + str(row) + " Time: " + str(end-start))
                output.close()  

