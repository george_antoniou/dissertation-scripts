#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from cassandra.cluster import Cluster
import csv
import time
import sys
import os.path
import random
from cassandra.query import BatchStatement
import threading

cluster6 = Cluster(['10.0.1.206'])
session6 = cluster6.connect('checkins')
cluster7 = Cluster(['10.0.1.207'])
session7 = cluster7.connect('checkins')
cluster8 = Cluster(['10.0.1.208'])
session8 = cluster8.connect('checkins')

read_statement6 = session6.prepare("""
                SELECT *
                FROM data
                WHERE id = ?""")
write_statement6 = session6.prepare("""
                UPDATE data
                SET accuracy = ?, place_id = ?, row_id = ?, time = ?, x = ?, y = ?
                WHERE id = ?""")

read_statement7 = session7.prepare("""
                SELECT *
                FROM data
                WHERE id = ?""")
write_statement7 = session7.prepare("""
                UPDATE data
                SET accuracy = ?, place_id = ?, row_id = ?, time = ?, x = ?, y = ?
                WHERE id = ?""")
read_statement8 = session8.prepare("""
                SELECT *
                FROM data
                WHERE id = ?""")
write_statement8 = session8.prepare("""
                UPDATE data
                SET accuracy = ?, place_id = ?, row_id = ?, time = ?, x = ?, y = ?
                WHERE id = ?""")

def read_write_query(session, read_statement, write_statement):
    reads = []
    writes = []
    start = time.time()

    for i in range(100):
        value = random.randrange(232944168)
        reads.append(session.execute(read_statement, [value]))             
        writes.append(session.execute(write_statement, (-reads[i][0][1], \
        -reads[i][0][2],-reads[i][0][3],-reads[i][0][4],\
        -reads[i][0][5],-reads[i][0][6],value)))                         
                                           
    end = time.time()
    print("Read/Write 100 time: " + str(end-start))  

t6 = threading.Thread(target=read_write_query, args=(session6,read_statement6,write_statement6, ))
t7 = threading.Thread(target=read_write_query, args=(session7,read_statement7,write_statement7, ))
t8 = threading.Thread(target=read_write_query, args=(session8,read_statement8,write_statement8 ))

t6.start()
t7.start()
t8.start()

