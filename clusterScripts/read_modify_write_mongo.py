#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from pymongo import MongoClient
import csv
import time
import sys
import os.path
import random
import threading

client6 = MongoClient('10.0.1.206',26051)
db6 = client6.checkins
client7 = MongoClient('10.0.1.207',26051)
db7 = client7.checkins
client8 = MongoClient('10.0.1.208',26051)
db8 = client8.checkins


def read_write_query(db):
    reads = []
    writes = []
    start = time.time()
    for i in range(100):
        value = random.randrange(232944168)
        reads.append(db.data.find({"_id":value}))
        row_id = 0 -reads[i][0]["row_id"]
        accuracy = 0 -reads[i][0]["accuracy"]
        x = 0 -reads[i][0]["x"]
        y = 0 -reads[i][0]["y"]
        _time = 0 -reads[i][0]["time"]
        place_id = 0 -reads[i][0]["place_id"] 
        writes.append(db.data.update_one({"_id":value},
                                     {"$set":{"row_id":row_id,
                                             "accuracy":accuracy,
                                             "x":x,
                                             "y":y,
                                             "time":_time,
                                             "place_id":place_id}}))

       
    end = time.time()
    print("Read/Write 100: " + str(end-start))

t6 = threading.Thread(target=read_write_query, args=(db6, ))
t7 = threading.Thread(target=read_write_query, args=(db7, ))
t8 = threading.Thread(target=read_write_query, args=(db8, ))
t6.start()
t7.start()
t8.start()


