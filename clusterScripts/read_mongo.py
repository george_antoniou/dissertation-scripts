#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from pymongo import MongoClient
import csv
import time
import sys
import os.path
import random
import threading

client6 = MongoClient('10.0.1.206',26051)
db6 = client6.checkins
client7 = MongoClient('10.0.1.207',26051)
db7 = client7.checkins
client8 = MongoClient('10.0.1.208',26051)
db8 = client8.checkins


results = []
def read_query(db):
    start = time.time()
    for i in range(100):
        value = random.randrange(232944167)
        results.append(db.data.find_one({"_id":value}))
    end = time.time()
    print("Read 1K time: " + str(end-start))

t6 = threading.Thread(target=read_query, args=(db6, ))
t7 = threading.Thread(target=read_query, args=(db7, ))
t8 = threading.Thread(target=read_query, args=(db8, ))
t6.start()
t7.start()
t8.start()
t6.join()
t7.join()
t8.join()

output = open ("temp_mongo.txt", 'w')

for i in range(30):
    output.write(str(results[i]))

output.close()  

