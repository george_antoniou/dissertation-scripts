#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from cassandra.cluster import Cluster
import csv
import time
import sys
import os.path
import random
import threading
from cassandra.query import BatchStatement

cluster6 = Cluster(['10.0.1.206'])
session6 = cluster6.connect('checkins')
cluster7 = Cluster(['10.0.1.207'])
session7 = cluster7.connect('checkins')
cluster8 = Cluster(['10.0.1.208'])
session8 = cluster8.connect('checkins')

statement6 =session6.prepare( """
                UPDATE data
                SET row_id = ?, accuracy = ?, x = ?, y = ?, time = ?, place_id = ?
                WHERE id = ?""")

statement7 =session7.prepare( """
                UPDATE data
                SET row_id = ?, accuracy = ?, x = ?, y = ?, time = ?, place_id = ?
                WHERE id = ?""")
statement8 =session8.prepare( """
                UPDATE data
                SET row_id = ?, accuracy = ?, x = ?, y = ?, time = ?, place_id = ?
                WHERE id = ?""")

def write_query(session, statement):
    start = time.time()
    for i in range(100):
        value = random.randrange(232944168)
        results = session.execute(statement, (-1,-1, -1.0, -1.0, -1, -1, value))
                                           
    end = time.time()
    print("Write 100 values: " + str(end-start))

t6 = threading.Thread(target=write_query, args=(session6,statement6, ))
t7 = threading.Thread(target=write_query, args=(session7,statement7, ))
t8 = threading.Thread(target=write_query, args=(session8,statement8, ))
t6.start()
t7.start()
t8.start()
  

