#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from pymongo import MongoClient
import csv
import time
import sys
import os.path
import random
import threading

client6 = MongoClient('10.0.1.206',26051)
db6 = client6.checkins
client7 = MongoClient('10.0.1.207',26051)
db7 = client7.checkins
client8 = MongoClient('10.0.1.208',26051)
db8 = client8.checkins

def write_query(db):
    start = time.time()
    for i in range(100):
        value = random.randrange(232944168)
        results = db.data.update_one({"_id":value},
                                 {"$set":{"row_id":-1,
                                         "accuracy":-1,
                                         "x":-1.0,
                                         "y":-1.0,
                                         "time":-1,
                                         "place_id":-1}})
                                      
    end = time.time()
    print("Write 100: " + str(end-start))

t6 = threading.Thread(target=write_query, args=(db6, ))
t7 = threading.Thread(target=write_query, args=(db7, ))
t8 = threading.Thread(target=write_query, args=(db8, ))
t6.start()
t7.start()
t8.start()


