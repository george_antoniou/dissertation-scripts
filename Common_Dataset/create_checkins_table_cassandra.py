from cassandra.cluster import Cluster

cluster = Cluster()
session = cluster.connect('checkins')

results = session.execute("""
    CREATE  TABLE data(
       ID INT PRIMARY KEY,
       ROW_ID INT,
       X FLOAT,
       Y FLOAT,
       ACCURACY INT, 
       TIME INT,
       PLACE_ID BIGINT,
       )""")

        
