#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from cassandra.cluster import Cluster
import csv
import time
import sys
import os.path
import random
from cassandra.query import BatchStatement

cluster = Cluster()
session = cluster.connect('checkins')
start = time.time()

save_path = '/home/george/scripts/Common_Dataset/cassandra_times/'

statement = session.prepare("""
                SELECT *
                FROM data
                WHERE id = ?""")


results = []
for i in range(1000):
    value = random.randrange(232944168)
    results.append(session.execute(statement, [value]))
                       
end = time.time()
#filename = os.path.join(save_path, "read_cassandra.txt" )
#output = open (filename, 'w')
#output.write("Read 1K time: " + str(end-start))
print("Read 1K time: " + str(end-start))
#output.close()  

