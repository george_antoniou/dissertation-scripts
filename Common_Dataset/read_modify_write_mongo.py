#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from pymongo import MongoClient
import csv
import time
import sys
import os.path
import random

client = MongoClient()
db = client.checkins
start = time.time()

save_path = '/home/george/scripts/Common_Dataset/mongo_times/'

reads= []
writes = []

for i in range(1000):
    value = random.randrange(232944168)
    reads.append(db.data.find({"_id":value}))
    row_id = 0 -reads[i][0]["row_id"]
    accuracy = 0 -reads[i][0]["accuracy"]
    x = 0 -reads[i][0]["x"]
    y = 0 -reads[i][0]["y"]
    _time = 0 -reads[i][0]["time"]
    place_id = 0 -reads[i][0]["place_id"] 
    writes.append(db.data.update_one({"_id":value},
                                 {"$set":{"row_id":row_id,
                                         "accuracy":accuracy,
                                         "x":x,
                                         "y":y,
                                         "time":_time,
                                         "place_id":place_id}}))

                                      
end = time.time()
print("Read/Write 1K: " + str(end-start))
filename = os.path.join(save_path, "read_modify_write_mongo.txt" )
output = open (filename, 'w')
for document in reads:    
    output.write(str(document[0]))
output.close()  

