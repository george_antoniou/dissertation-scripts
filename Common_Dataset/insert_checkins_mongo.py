from pymongo import MongoClient
import json
import sys
import csv
import time
import os

client = MongoClient()
db = client.checkins

start = time.time()
row = 0

save_path = '/home/george/scripts/Common_Dataset/mongo_times/'


for i in range(8):
    with open ('train.csv', 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        next(reader,None)
        for data in reader:
            document = '{ "_id" : ' + str(row) + ',' + \
                        ' "row_id":' + data[0] + ',' + \
                        ' "x":' + data[1] + ',' + \
                        ' "y":' + data[2] + ',' + \
                        ' "accuracy":' + data[3] + ','+ \
                        ' "time":' + data[4] + ',' +  \
                        ' "place_id":' + data[5] + '}'
                    
           
            db.data.insert_one(json.loads(document))
            row = row + 1

            if (row % 1000000 == 0):
                end = time.time()
                filename = os.path.join(save_path, "row_"+str(row)+".txt")
                output = open (filename, 'w')
                output.write("Row: " + str(row) + " Time: " + str(end-start))
                output.close()
            



