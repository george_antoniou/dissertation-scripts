#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from cassandra.cluster import Cluster
import csv
import time
import sys
import os.path
import random
from cassandra.query import BatchStatement

cluster = Cluster()
session = cluster.connect('checkins')
start = time.time()

save_path = '/home/george/scripts/Common_Dataset/cassandra_times/'

read_statement = session.prepare("""
                SELECT *
                FROM data
                WHERE id = ?""")
write_statement = session.prepare("""
                UPDATE data
                SET accuracy = ?, place_id = ?, row_id = ?, time = ?, x = ?, y = ?
                WHERE id = ?""")
reads = []
writes = []

for i in range(1000):
    value = random.randrange(232944168)
    reads.append(session.execute(read_statement, [value]))             
    writes.append(session.execute(write_statement, (-reads[i][0][1], \
    -reads[i][0][2],-reads[i][0][3],-reads[i][0][4],\
    -reads[i][0][5],-reads[i][0][6],value)))
                                           
end = time.time()
print("Read/Write 1K time: " + str(end-start))
filename = os.path.join(save_path, "read__modify_write_cassandra.txt" )
output = open (filename, 'w')
for i in range(1000):
    output.write(str(writes[i]) + "\n")
output.close()  

