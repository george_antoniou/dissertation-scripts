#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from pymongo import MongoClient
import csv
import time
import sys
import os.path
import random

client = MongoClient()
db = client.checkins
start = time.time()

save_path = '/home/george/scripts/Common_Dataset/mongo_times/'

for i in range(1000):
    value = random.randrange(232944168)
    results = db.data.update_one({"_id":value},
                                 {"$set":{"row_id":-1,
                                         "accuracy":-1,
                                         "x":-1.0,
                                         "y":-1.0,
                                         "time":-1,
                                         "place_id":-1}})
                                      
end = time.time()
print("Write 1K: " + str(end-start))
#filename = os.path.join(save_path, "write_mongo.txt" )
#output = open (filename, 'w')
#output.write("Write 1M time: " + str(end-start))
#output.close()  

