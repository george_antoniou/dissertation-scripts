#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from cassandra.cluster import Cluster
import csv
import time
import sys
import os.path
import random
from cassandra.query import BatchStatement


cluster = Cluster()
session = cluster.connect('checkins')
start = time.time()

save_path = '/home/george/scripts/Common_Dataset/cassandra_times/'
statement =session.prepare( """
                UPDATE data
                SET row_id = ?, accuracy = ?, x = ?, y = ?, time = ?, place_id = ?
                WHERE id = ?""")

for i in range(1000):
    value = random.randrange(232944168)
    results = session.execute(statement, (-1,-1, -1.0, -1.0, -1, -1, value))
                                           
end = time.time()
print("Write 1K values: " + str(end-start))
#filename = os.path.join(save_path, "write_cassandra.txt" )
#output = open (filename, 'w')
#output.write("Write 1M time: " + str(end-start))
#output.close()  

