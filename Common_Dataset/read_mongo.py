#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from pymongo import MongoClient
import csv
import time
import sys
import os.path
import random

client = MongoClient()
db = client.checkins
start = time.time()

save_path = '/home/george/scripts/Common_Dataset/mongo_times/'
results = []
for i in range(1000):
    value = random.randrange(232944167)
    results.append(db.data.find_one({"_id":value}))

print(results[0])
end = time.time()
print("Read 1K time: " + str(end-start))

filename = os.path.join(save_path, "temp_mongo.txt" )
output = open (filename, 'w')

for i in range(1000):
    output.write(str(results[i]))

output.close()  
end = time.time()
print("read 1K time: " + str(end-start))
