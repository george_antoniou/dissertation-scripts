from pymongo import MongoClient
from  bson.code import Code

client = MongoClient()
db = client.twitter

map = Code("""function () {
              emit(this.lang, 1);
              };
           """)

reduce = Code("""function(keyLanguage, valueOccurences){
                 
                  totalOccurences = 0;

                 for (i=0; i<valueOccurences.length; i++)
                     totalOccurences += valueOccurences[i];

                 return(totalOccurences); 
                 };
              """)
result = db.tweets.map_reduce(map,reduce,"results")
for document in  result.find():
    print(document)


