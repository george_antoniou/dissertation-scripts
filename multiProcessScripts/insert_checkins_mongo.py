from pymongo import MongoClient
import json
import sys
import csv
import time
import os
import multiprocessing

client = MongoClient()
db = client.test
db.data.remove({})
def query(id):
    client = MongoClient()
    db = client.test
    row = id * 100000
    start = time.time()
    with open ('train.csv', 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        next(reader,None)
        for data in reader:
            document = '{ "_id" : ' + str(row) + ',' + \
                        ' "row_id":' + data[0] + ',' + \
                        ' "x":' + data[1] + ',' + \
                        ' "y":' + data[2] + ',' + \
                        ' "accuracy":' + data[3] + ','+ \
                        ' "time":' + data[4] + ',' +  \
                        ' "place_id":' + data[5] + '}'
                                            
           
            res = db.data.insert_one(json.loads(document))
            row = row + 1

            if (row % 100000 == 0):
                end = time.time()
                print(str(end-start))
                break



jobs = []
for i in range(int(sys.argv[1])):
    p = multiprocessing.Process(target=query, args = (i,))
    jobs.append(p)
    p.start()

