#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from cassandra.cluster import Cluster
import csv
import time
import sys
import os.path
import random
from cassandra.query import BatchStatement
import multiprocessing

def query(id):
    cluster = Cluster()
    session = cluster.connect('checkins')
    start = time.time()
    statement =session.prepare( """
                UPDATE data
                SET row_id = ?, accuracy = ?, x = ?, y = ?, time = ?, place_id = ?
                WHERE id = ?""")

    for i in range(100000):
        value = random.randrange(232944168)
        results = session.execute(statement, (-1,-1, -1.0, -1.0, -1, -1, value))
                                           
    end = time.time()
    print(str(end-start))
 

jobs = []
for i in range(int(sys.argv[1])):
    p = multiprocessing.Process(target=query, args = (i,))
    jobs.append(p)
    p.start()

