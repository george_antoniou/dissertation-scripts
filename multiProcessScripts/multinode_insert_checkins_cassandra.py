#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from cassandra.cluster import Cluster
import csv
import time
import sys
import os.path
import multiprocessing
from cassandra.query import BatchStatement


def query(id,starting_record):
    cluster = Cluster(['10.0.1.206'])
    session = cluster.connect('test')
    statement = session.prepare("""
                INSERT INTO data(
                ID,
                ROW_ID,
                X,
                Y,
                TIME,
                ACCURACY,
                PLACE_ID)
                VALUES
                (?,?,?,?,?,?,?)
                """)

    row = id * 100000 + starting_record 
    results = []
    with open ('train.csv', 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        next(reader,None)
        start = time.time()
        for data in reader:
           
            results.append(session.execute(statement,
                [row,int(data[0]),float(data[1]),float(data[2]),int(data[3]),
                int(data[4]),int(data[5])]))                       
            row = row + 1
            
            if (row % 100000 == 0):
                end = time.time()
                print(str(end-start))
                break

jobs = []
start = int(sys.argv[1]) * int(sys.argv[2]) * 100000
for i in range(int(sys.argv[2])):
    p = multiprocessing.Process(target=query, args = (i,start))
    jobs.append(p)
    p.start()
