#cqlsh --request-timeout 120 127.0.0.1 --> for count(*)
from pymongo import MongoClient
import csv
import time
import sys
import os.path
import random
import multiprocessing

def query(id):
    client = MongoClient()
    db = client.checkins
    start = time.time()

    for i in range(100):
        value = random.randrange(232944168)
        results = db.data.update_one({"_id":value},
                                 {"$set":{"row_id":-1,
                                         "accuracy":-1,
                                         "x":-1.0,
                                         "y":-1.0,
                                         "time":-1,
                                         "place_id":-1}})
                                      
    end = time.time()
    print( str(end-start))

jobs = []
for i in range(int(sys.argv[1])):
    p = multiprocessing.Process(target=query, args = (i,))
    jobs.append(p)
    p.start()
 

